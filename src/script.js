const container = document.getElementById('favorite__teachers');
        const moveLeftButton = document.getElementById('moveLeft');
        const moveRightButton = document.getElementById('moveRight');

        const scrollAmount = 220; 
        moveLeftButton.addEventListener('click', () => {
            container.scrollLeft -= scrollAmount;
        });

        moveRightButton.addEventListener('click', () => {
            container.scrollLeft += scrollAmount;
        });